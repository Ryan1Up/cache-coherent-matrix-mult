#include <iostream>
#include <pthread.h>
#include <cstdlib>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <string>
using namespace std;


pthread_mutex_t lock;
double** A;
double** B;
double** C;
int thread_count;
int p, q, r;
void* computeMatrix(void* args);

void initArr(double** arr, int rows, int columns);
void zeroArr(double** arr, int rows, int columns);
void printArr(double** arr, int rows, int columns);

int main(int argc, char** argv){

    srand(time(NULL));

    thread_count = stoi(argv[4]);

    p = stoi(argv[1]);
    q = stoi(argv[2]);
    r = stoi(argv[3]);
    int* ranks = new int[thread_count];
    pthread_t* threads = new pthread_t[thread_count];

    A = new double*[p];
    B = new double*[q];
    C = new double*[p];
    for(int i = 0; i < p; i++){
        for(int j = 0; j < q; j++){
            B[j] = new double[r];
        }
        A[i] = new double[q];
        C[i] = new double[r];
    }

    initArr(A, p, q);
    initArr(B, q, r);
    zeroArr(C, p, r);

    printf("Array A (%dx%d): \n", p, q);
    printArr(A, p, q);
    printf("\n\nArray B (%dx%d): \n", q, r);
    printArr(B, q, r);
    printf("\n\nArray C (%dx%d): \n", p, r);
    printArr(C, p, r);


    for(int i = 0; i < thread_count; i++){
        ranks[i] = i;
        pthread_create(&threads[i], NULL, computeMatrix, (void*) &ranks[i]);
    }

    for(int j = 0; j < thread_count; j++){
        pthread_join(threads[j], NULL);
    }

    printArr(C, p, r);
    delete [] A; delete [] B; delete [] C; delete [] threads;
    return 0;
}


void initArr(double** arr, int rows, int columns){
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < columns; j++){
            arr[i][j] = 10.0 * ((double) rand() / (RAND_MAX));
            
        }
        printf("\n");
    }
}

void zeroArr(double** arr, int rows, int columns){
    for(int i = 0; i < rows; i++){
        for(int j = 0; j < columns; j++){
            arr[i][j] = 0;
        }
    }
}


void printArr(double** arr, int rows, int columns){
    
    printf("Printing Array: %p\n", (void*) arr);

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < columns; j++){
            if(j == columns - 1){
                printf("%.3f  ", arr[i][j]);
            }
            else
                printf("%.3f  |  ", arr[i][j]);

        }
        printf("\n");
    }
}

/////////////////////////////////////
// computeMatrix computes the dot product of two compatible matrices.
// It leverages the fact that every entry in row i of Matrix C contains
// in each summation element the entry A[i][x] where i is the shared row
// of matrices A and C.
// e.g
//  C[0][0] = A[0][0]*B[0][0] + A[0][1]*B[1][0] + A[0][2]*B[2][0]
//  C[0][1] = A[0][0]*B[0][1] + A[0][1]*B[1][1] + A[0][2]*B[2][2]
//  C[0][2] = A[0][0]*B[0][2] + A[0][1]*B[1][2] + A[0][2]*B[2][2]
//
// We can see that for every entry in row 0 of matrix C, the first element in the 
// summation equation starts with A[0][0]
// Further more, we see that as we go from C[0][0] to C[0][2] the corresponding elements from B,
// by addition 'column', are from the same row of B.
//
// Compute matrix leverages this property to maintain a higher cache hit rate by keeping the same row of a matrix
// in memory between iterations of the given for-loops. As cache's are row-major access this would serve to produce
// a higher performance
void* computeMatrix(void* args){

    int* rank = (int*) args;
    pthread_mutex_lock(&lock);
    printf("Thread Rank: %d\n", *rank);
    pthread_mutex_unlock(&lock);
    int  a_c_row = *rank;
    //p defines rows of output matrix C
    // Use rotation way, each thread jumps a number of rows, eliminates need for start~end calculations
    while(a_c_row < p){
        pthread_mutex_lock(&lock);
        printf("T_%d   Loading Matrix A and C row %d into cache \n", *rank%thread_count, a_c_row);
        pthread_mutex_unlock(&lock);
        //q defines rows of Matrix B
        for(int row = 0; row < q; row++){
            // r defines columns of output Matrix C
            pthread_mutex_lock(&lock);
            printf("T_%d   Loading Matrix B row %d into cache \n", *rank%thread_count, row);
            pthread_mutex_unlock(&lock);
            for(int col = 0; col < r; col++){
                C[a_c_row][col] += A[a_c_row][row]*B[row][col];
            }
        }

        a_c_row += thread_count;
    }
    return NULL;
}